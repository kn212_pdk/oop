﻿using System;
using lab10;

namespace lab10Csharrk
{
    class Program
    {
        static void Main(string[] args)
        {
            float drop1 = 16.32F;
            float drop2 = 13.22F;
            float drop3 = 16.32F;
            float[] drop11 = new float[10];
            drop11[0] = 1;
            drop11[1] = 6;
            drop11[3] = 3;
            drop11[4] = 2;
            double drop4 = 0;
            lab10.Fraction g = new lab10.Fraction();
            g.funcone(drop2);
            g.functwo(drop2, drop3);
            g.functhe(drop1, drop2, drop3);
            g.funcfo(drop1, drop4);
            g.funcf(drop11);
            g.funcc(drop11);
        }
    }
}
