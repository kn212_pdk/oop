﻿using System;
using System.Text;

namespace lab06Cshark
{
    
    class  Result
    {
        
        protected string[] Subject = new string[10];
        protected string[] Teacher = new string[10];
        protected double[] Point = new double[10];
        public Result(string[] subject,string[] teacher, double[] point)
        {
            for(int i = 0; i < 5; i++)
            {
                Subject[i] = subject[i];
                Teacher[i] = teacher[i];
                Point[i] = point[i];
            }

        }
        public void CoutResult()
        {
            
            for(int i = 0; i < 5; i++)
            {
                Console.WriteLine("Предмет: {0}", Subject[i]);
                Console.WriteLine("Вчитель: {0}", Teacher[i]);
                Console.WriteLine("Оцiнка: {0}", Point[i]);
                break;
            }
            
            
            
           
        }
        public void CoutNameStudent()
        {
            for (int i = 0; i < 2; i++)
            {
                Console.WriteLine("{0} - {1}",i, Subject[i]);
            }
        }
        public void CoutGroupStudent()
        {
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("{0} - {1}", i, Point[i]);
            }
        }
        public void GetAveragePoints()
        {
            double sumavr = 0;
            double jik = 0;
            for (int i = 0; i < 5; i++)
            {
                sumavr = sumavr + Point[i];
                jik++;
                
                
            }
            sumavr = sumavr / jik;
            Console.WriteLine("Середнє значення: {0}", sumavr);
        }
        public void GetBestSubject()
        {
            double max = 0;
            string subm = "";
            for (int i = 0; i < 5; i++)
            {
                if (max < Point[i])
                {
                    max = Point[i];
                    subm = Subject[i];
                }


            }
            Console.WriteLine("Предмет: {0}", subm);
            Console.WriteLine("Максимальний бал: {0}", max);
        }
        public void GetWorstSubject()
        {
            double min = 1000;
            string subm = "";
            for (int i = 0; i < 5; i++)
            {
                if (min > Point[i])
                {
                    min = Point[i];
                    subm = Subject[i];
                }


            }
            Console.WriteLine("Предмет: {0}", subm);
            Console.WriteLine("Найгірший бал: {0}", min);
        }
    }
    class Student
    {
        
        protected string[] Name = new string[10];
        protected string[] Surname = new string[10];
        protected string[] Group = new string[10];
        protected double[] Year = new double[10];
        protected string[] Results = new string[10];

        public Student(string[] name, string[] surname, string[] group, double[] year, string[] results)
        {
            for(int i = 0; i < 5; i++)
            {
                Name[i] = name[i];
                Surname[i] = surname[i];
                Group[i] = group[i];
                Year[i] = year[i];
                Results[i] = results[i];
            }
        }
        public void CoutStudent()
        {   
           
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Ім'я: {0}", Name[i]);
                Console.WriteLine("Призвище: {0}", Surname[i]);
                Console.WriteLine("Група: {0}", Group[i]);
                Console.WriteLine("Курс: {0}", Year[i]);
                Console.WriteLine("Результат: {0}", Results[i]);
                
                break;
            }
            
            
        }
        

    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = UTF8Encoding.UTF8;
            string[] sub = new string[20];
            string[] tech = new string[20];
            double[] poi = new double[20];
            sub[0] = "Математика";
            sub[1] = "ООП";
            tech[0] = "Якийсь Олександер";
            tech[1] = "Фурiхата Денис Вiкторович";
            poi[0] = 97;
            poi[1] = 100;
            sub[2] = "Математика";
            sub[3] = "ООП";
            tech[2] = "Якийсь Олександер";
            tech[3] = "Фурiхата Денис Вiкторович";
            poi[2] = 95;
            poi[3] = 100;
            sub[4] = "Математика";
            sub[5] = "ООП";
            tech[4] = "Якийсь Олександер";
            tech[5] = "Фурiхата Денис Вiкторович";
            poi[4] = 97;
            poi[5] = 100;
            string[] nam = new string[20];
            string[] snam = new string[20];
            string[] gryp = new string[20];
            double[] yer = new double[20];
            string[] rez = new string[20];
            nam[0] = "Levi";
            nam[2] = "Олександр";
            nam[4] = "Міша";
            snam[0] = "Akkamаn";
            snam[2] = "Костилєв";
            snam[4] = "Малишев";
            gryp[0] = "КН-21-2";
            gryp[2] = "КН-21-1";
            gryp[4] = "КН-21-3";
            yer[0] = 1;
            yer[2] = 1;
            yer[4] = 1;
            rez[0] = "*****************";
            rez[2] = "*****************";
            rez[4] = "*****************";

            for (int i = 0; i < 1; i++)
            {
                Student student = new Student(nam, snam, gryp, yer, rez);
                student.CoutStudent();
                
                Result result = new Result(sub,tech,poi);
                result.CoutResult();
                result.GetAveragePoints();
                result.GetBestSubject();
                result.GetWorstSubject();
            }
            
            double a;
            Console.WriteLine("Введіть що потрібно копіювати -->");
            Console.WriteLine("1 - Назву предмету");
            Console.WriteLine("2 - Оцінки");
            a = Convert.ToInt32(Console.ReadLine());
            switch (a)
            {
                case 1:
                    Console.WriteLine("Виберіть предмет:");
                    for(int i = 0; i < 1; i++)
                    {
                        Result result = new Result(sub, tech, poi);
                        result.CoutNameStudent(); 
                    }
                    double oneco = Convert.ToDouble(Console.ReadLine());
                    if(oneco == 0)
                    {
                        string[] copyt = new string[2];
                        copyt[0] = sub[0];
                        Console.WriteLine("Виберіть заміну:");
                        
                        double voneco = Convert.ToDouble(Console.ReadLine());
                        if (voneco == 0)
                        {
                            sub[0] = copyt[0];
                        }
                        if (voneco == 1)
                        {
                            sub[1] = copyt[0];
                        }
                        if (voneco == 2)
                        {
                            sub[2] = copyt[0];
                        }
                        if (voneco == 3)
                        {
                            sub[3] = copyt[0];
                        }
                       
                        for (int i = 0; i < 1; i++)
                        {
                            Result result = new Result(sub, tech, poi);
                            result.CoutNameStudent();
                        }
                        
                        
                        
                    }
                    if (oneco == 1)
                    {
                        string[] copyt = new string[2];
                        copyt[0] = sub[1];
                        Console.WriteLine("Виберіть заміну:");
                        
                        double voneco = Convert.ToDouble(Console.ReadLine());
                        if (voneco == 0)
                        {
                            sub[0] = copyt[0];
                        }
                        if (voneco == 1)
                        {
                            sub[1] = copyt[0];
                        }
                        if (voneco == 2)
                        {
                            sub[2] = copyt[0];
                        }
                        if (voneco == 3)
                        {
                            sub[3] = copyt[0];
                        }
                        for (int i = 0; i < 1; i++)
                        {
                            Result result = new Result(sub, tech, poi);
                            result.CoutNameStudent();
                        }
                    }
                    break;
                case 2:
                    Console.WriteLine("Виберіть оцінку:");
                    for (int i = 0; i < 1; i++)
                    {
                        Result result = new Result(sub, tech, poi);
                        result.CoutGroupStudent();
                    }
                        double onecoo = Convert.ToDouble(Console.ReadLine());
                        if (onecoo == 0)
                        {
                            double[] copyty = new double[2];
                            copyty[0] = poi[0];
                            Console.WriteLine("Виберіть заміну:");
                            double voneco = Convert.ToDouble(Console.ReadLine());
                            if (voneco == 0)
                            {
                                poi[0] = copyty[0];
                            }
                            if (voneco == 1)
                            {
                                poi[1] = copyty[0];
                            }
                            if (voneco == 2)
                            {
                                poi[2] = copyty[0];
                            }
                            if (voneco == 3)
                            {
                                poi[3] = copyty[0];
                            }
                            Result result = new Result(sub, tech, poi);
                            result.CoutGroupStudent();
                        }
                        if (onecoo == 1)
                        {
                            double[] copyty = new double[2];
                            copyty[0] = poi[1];
                            Console.WriteLine("Виберіть заміну:");
                            double voneco = Convert.ToDouble(Console.ReadLine());
                            if (voneco == 0)
                            {
                                poi[0] = copyty[0];
                            }
                            if (voneco == 1)
                            {
                                poi[1] = copyty[0];
                            }
                            if (voneco == 2)
                            {
                                poi[2] = copyty[0];
                            }
                            if (voneco == 3)
                            {
                                poi[3] = copyty[0];
                            }
                            Result result = new Result(sub, tech, poi);
                            result.CoutGroupStudent();
                    }
                        if (onecoo == 2)
                        {
                            double[] copyty = new double[2];
                            copyty[0] = poi[2];
                            Console.WriteLine("Виберіть заміну:");
                            double voneco = Convert.ToDouble(Console.ReadLine());
                            if (voneco == 0)
                            {
                                poi[0] = copyty[0];
                            }
                            if (voneco == 1)
                            {
                                poi[1] = copyty[0];
                            }
                            if (voneco == 2)
                            {
                                poi[2] = copyty[0];
                            }
                            if (voneco == 3)
                            {
                                poi[3] = copyty[0];
                            }
                        Result result = new Result(sub, tech, poi);
                        result.CoutGroupStudent();
                    }
                        if (onecoo == 3)
                        {
                            double[] copyty = new double[2];
                            copyty[0] = poi[2];
                            Console.WriteLine("Виберіть заміну:");
                            double voneco = Convert.ToDouble(Console.ReadLine());
                            if (voneco == 0)
                            {
                                poi[0] = copyty[0];
                            }
                            if (voneco == 1)
                            {
                                poi[1] = copyty[0];
                            }
                            if (voneco == 2)
                            {
                                poi[2] = copyty[0];
                            }
                            if (voneco == 3)
                            {
                                poi[3] = copyty[0];
                            }
                        Result result = new Result(sub, tech, poi);
                        result.CoutGroupStudent();
                    
                        }
                    break;
                default:
                    Console.WriteLine("Введена цифра не відноситься ні до одного із заданих");
                    break;
                    
            }
                

        }
    }
}
