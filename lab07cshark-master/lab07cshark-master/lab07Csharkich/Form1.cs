﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab07Csharkich
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBoxMaterial_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxMaterial.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBoxMaterial.SelectedIndex == 0 && radioButton1.Checked == true)
            {
                label6.Visible = true;
                label7.Visible = true;
                double one = Convert.ToInt32(textBox1.Text);
                if (checkBox1.Checked == true)
                {
                    label7.Text = Convert.ToString(one * 100 + 50 + " $");
                }
                else
                {
                    label7.Text = Convert.ToString(one * 100 + " $");
                }
            }
            if (comboBoxMaterial.SelectedIndex == 0 && radioButton2.Checked == true)
            {
                label6.Visible = true;
                label7.Visible = true;
                double one = Convert.ToInt32(textBox1.Text);
                if (checkBox1.Checked == true)
                {
                    label7.Text = Convert.ToString(one * 150 + 50 + " $");
                }
                else
                {
                    label7.Text = Convert.ToString(one * 150 + " $");
                }
            }
            if (comboBoxMaterial.SelectedIndex == 1 && radioButton1.Checked == true)
            {
                label6.Visible = true;
                label7.Visible = true;
                double one = Convert.ToInt32(textBox1.Text);
                if (checkBox1.Checked == true)
                {
                    label7.Text = Convert.ToString(one * 160 + 50 + " $");
                }
                else
                {
                    label7.Text = Convert.ToString(one * 160 + " $");
                }
            }
            if (comboBoxMaterial.SelectedIndex == 1 && radioButton2.Checked == true)
            {
                label6.Visible = true;
                label7.Visible = true;
                double one = Convert.ToInt32(textBox1.Text);
                if (checkBox1.Checked == true)
                {
                    label7.Text = Convert.ToString(one * 200 + 50 + " $");
                }
                else
                {
                    label7.Text = Convert.ToString(one * 200 + " $");
                }
            }
            if (comboBoxMaterial.SelectedIndex == 2 && radioButton1.Checked == true)
            {
                label6.Visible = true;
                label7.Visible = true;
                double one = Convert.ToInt32(textBox1.Text);
                if (checkBox1.Checked == true)
                {
                    label7.Text = Convert.ToString(one * 120 + 50 + " $");
                }
                else
                {
                    label7.Text = Convert.ToString(one * 120 + " $");
                }
            }
            if (comboBoxMaterial.SelectedIndex == 2 && radioButton2.Checked == true)
            {

                label6.Visible = true;
                label7.Visible = true;
                double one = Convert.ToInt32(textBox1.Text);
                if (checkBox1.Checked == true)
                {
                    label7.Text = Convert.ToString(one * 180 + 50 + " $");
                }
                else
                {
                    label7.Text = Convert.ToString(one * 180 + " $");
                }

            }
        
        }
    }
}
