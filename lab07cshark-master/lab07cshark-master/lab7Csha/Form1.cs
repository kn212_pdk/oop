﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab7Csha
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBoxMaterial.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(comboBoxMaterial.SelectedIndex == 0 && radioButton1.Checked == true)
            {
                label6.Visible = true;
                label7.Visible = true;
                double one = Convert.ToInt32(textBox1.Text);
                double two = Convert.ToInt32(textBox2.Text);
                if (checkBox1.Checked == true)
                {
                    label7.Text = Convert.ToString((one * two) * 0.25 + 35 + " Грн");
                }
                else
                {
                    label7.Text = Convert.ToString((one * two) * 0.25 + " Грн");
                }
            }
            if (comboBoxMaterial.SelectedIndex == 0 && radioButton2.Checked == true)
            {
                label6.Visible = true;
                label7.Visible = true;
                double one = Convert.ToInt32(textBox1.Text);
                double two = Convert.ToInt32(textBox2.Text);
                if (checkBox1.Checked == true)
                {
                    label7.Text = Convert.ToString((one * two) * 0.30 + 35 + " Грн");
                }
                else
                {
                    label7.Text = Convert.ToString((one * two) * 0.30 + " Грн");
                }
            }
            if (comboBoxMaterial.SelectedIndex == 1 && radioButton1.Checked == true)
            {
                label6.Visible = true;
                label7.Visible = true;
                double one = Convert.ToInt32(textBox1.Text);
                double two = Convert.ToInt32(textBox2.Text);
                if (checkBox1.Checked == true)
                {
                    label7.Text = Convert.ToString((one * two) * 0.05 + 35 + " Грн");
                }
                else
                {
                    label7.Text = Convert.ToString((one * two) * 0.05 + " Грн");
                }
            }
            if (comboBoxMaterial.SelectedIndex == 1 && radioButton2.Checked == true)
            {
                label6.Visible = true;
                label7.Visible = true;
                double one = Convert.ToInt32(textBox1.Text);
                double two = Convert.ToInt32(textBox2.Text);
                if (checkBox1.Checked == true)
                {
                    label7.Text = Convert.ToString((one * two) * 0.10 + 35 + " Грн");
                }
                else
                {
                    label7.Text = Convert.ToString((one * two) * 0.10 + " Грн");
                }
            }
            if (comboBoxMaterial.SelectedIndex == 2 && radioButton1.Checked == true)
            {
                label6.Visible = true;
                label7.Visible = true;
                double one = Convert.ToInt32(textBox1.Text);
                double two = Convert.ToInt32(textBox2.Text);
                if (checkBox1.Checked == true)
                {
                    label7.Text = Convert.ToString((one * two) * 0.15 + 35 + " Грн");
                }
                else
                {
                    label7.Text = Convert.ToString((one * two) * 0.15 + " Грн");
                }
            }
            if (comboBoxMaterial.SelectedIndex == 2 && radioButton2.Checked == true)
            {
                
                label6.Visible = true;
                label7.Visible = true;
                double one = Convert.ToInt32(textBox1.Text);
                double two = Convert.ToInt32(textBox2.Text);
                if(checkBox1.Checked == true)
                {
                    label7.Text = Convert.ToString((one * two) * 0.20 + 35 + " Грн");
                }
                else
                {
                    label7.Text = Convert.ToString((one * two) * 0.20 + " Грн");
                }
                
            }
        }
    }
}
